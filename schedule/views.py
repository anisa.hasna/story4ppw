from django.shortcuts import render, redirect

from .forms import Schedule_Form
from .models import Schedule_Model

# Create your views here.
def delete(request, delete_id):
	Schedule_Model.objects.filter(id=delete_id).delete()
	return redirect('schedule:display')

def index(request):
	scheduleform = Schedule_Form(request.POST or None)

	if request.method == 'POST':
		if scheduleform.is_valid():
			scheduleform.save()

			return redirect('schedule:display')

		#Schedule_Model.objects.create(
		#	tanggal			= request.POST['tanggal'],
			#jam				= request.POST['jam'],
		#	nama_kegiatan	= request.POST['nama_kegiatan'],
		#	tempat			= request.POST['tempat'],
		#	kategori		= request.POST['kategori'],
		#)

	context = {
		'data_form': scheduleform,
	}
	return render(request,'schedule/scheduleforms.html', context)

def create(request):
	schedulemodel = Schedule_Model.objects.all()

	context = {
		'judul': 'Jadwal Kegiatan',
		'data_model': schedulemodel,
	}

	return render(request,'schedule/scheduledisplay.html', context)


	