from django.urls import path, re_path

from . import views

urlpatterns = [
	path('delete/<int:delete_id>/', views.delete, name='delete'),
	path('fill/', views.index, name='index'),
    path('', views.create, name='display'), 
]