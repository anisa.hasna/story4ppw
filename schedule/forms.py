from django import forms
from .models import Schedule_Model

class Schedule_Form(forms.ModelForm):
	class Meta:
		model = Schedule_Model
		fields =[
			'tanggal',
			'nama_kegiatan',
			'tempat',
			'kategori',
		]
		labels ={
			'tanggal': 'Tanggal dan Waktu \n (YYYY-MM-DD HH:MM) ',
		}

	#tanggal = forms.DateField(
	#		widget = forms.DateInput)
	#jam = forms.TimeField()
	#nama_kegiatan = forms.CharField()
	#tempat = forms.CharField()
	#pilihan_kategori = (
	#	('kuliah', 'Kuliah'),
	#	('bem', 'BEM'),
	#	('kepanitiaan', 'Kepanitiaan'),
	#	('keluarga', 'Keluarga'),
	#	('main', 'Main'),
	#	('lainnya', 'Lainnya'),
	#)
	#kategori = forms.ChoiceField(choices=pilihan_kategori)