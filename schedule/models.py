from django.db import models

class Schedule_Model(models.Model):
    
    tanggal = models.DateTimeField()
    #jam = models.TimeField()
    nama_kegiatan = models.CharField(max_length=50)
    tempat = models.CharField(max_length=100)
    pilihan_kategori = (
    	('Kuliah', 'Kuliah'),
    	('BEM', 'BEM'),
    	('Kepanitiaan', 'Kepanitiaan'),
    	('Keluarga', 'Keluarga'),
        ('Main', 'Main'),
        ('Lainnya', 'Lainnya'),
    )
    kategori = models.CharField(max_length=15, choices=pilihan_kategori)

    def __str__(self):
        return "{}. {}".format(self.id, self.tanggal)